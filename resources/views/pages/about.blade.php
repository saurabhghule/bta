@extends('index')

@section('content')
	
	<section class="about-page section banner-bg m-t-50 border-b-15">
		<div class="container banner-content">
			<h2 class="m-t-150 color-white">We exist to fight human trafficking through helping raise awareness, prevention & aftercare.</h2>
			<div class="about-container">
				<h3 class="color-white m-tb-30">Our product line not only empowers people to Carry the Cause and raise awareness, 
					but it also funds our anti-trafficking work as every product is connected to impact 
					levels ranging from awareness efforts, to youth education, to survivor aftercare 
					and empowerment.
				</h3>
				<h5 class="color-white">Eye Heart World was born in 2010 when Brian and Season Russo decided to take 
					action against human trafficking in an innovative way. For the first awareness 
					event they organized, Season and her mother, Denise Foster, created 30 bags, 
					hoping to raise funds for a safe home in their community. Supporting the cause 
					in a tangible way proved to be contagious - every single bag was sold that day. 
					Since it’s inception, Eye Heart World has sold thousands of products; educated 
					over 20,000 individuals and raised over $100,000 to fund the fight against 
					Human Trafficking.
				</h5>
			</div>
		</div>
	</section>

	<section class="about-page section section-1 valign sm-p-tb-75">
		<div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <div class="section-content text-left">
                        <h1>What is the <span class="color-yellow">Orange Rose?</span></h1>
                        <!-- <a href="" class="p-15-25 border-1-black m-t-20 color-black cta-btn ">SUPPORT NOW</a> -->
                        <h4 class="m-t-20">
                        	When the first Eye Heart World handbags were created, 
                        	we set out to make a statement that would shine a light 
                        	on the plight of millions around the world. We placed a
                        	symbolic orange rose on all of our handbags that represents 
                        	the delicate and unique individual lives of those who are 
                        	exploited. Each life deserves freedom - so when it's in your 
                        	hand or on your shoulder, you don't just carry a bag, you Carry the Cause.
                        </h4>
                    </div>
                </div>
            </div>
        </div>
	</section>

	<section class="about-page section section-2 p-tb-75 bgColor-lightGray">
		<div class="container">
			<div class="row">
                <div class="col-sm-12">
                    <div class="section-header m-b-25">
                        <h3 class="no-margin">MEET THE TEAM</h3>
                    </div>
                </div>
            </div>

            <div class="team-grid">
            	<div class="row">
	            	<div class="col-sm-12 col-md-4">
	            		<div class="team-card">
	            			<img src="/img/about/founder-1.jpg">

	            			<div class="team-desc valign bgColor-black-000">
	            				<div class="founder-desc text-left">
		            				<h3 class="no-margin color-yellow">Brian Russo</h3>
		            				<p class="no-margin-b color-white">Creative Director</p>
		            			</div>
		            			<div class="founder-socials">
		            				<i class="fa fa-envelope font-25 color-white"></i>
		            			</div>
	            			</div>
	            		</div>
	            	</div>

	            	<div class="col-sm-12 col-md-4">
	            		<div class="team-card">
	            			<img src="/img/about/founder-2.jpg">

	            			<div class="team-desc valign bgColor-black-000">
	            				<div class="founder-desc text-left">
		            				<h3 class="no-margin color-yellow">Season Russo</h3>
		            				<p class="no-margin-b color-white">Program Director</p>
		            			</div>
		            			<div class="founder-socials">
		            				<i class="fa fa-envelope font-25 color-white"></i>
		            			</div>
	            			</div>
	            		</div>
	            	</div>

	            	<div class="col-sm-12 col-md-4">
	            		<div class="team-card">
	            			<img src="/img/about/founder-3.jpg">

	            			<div class="team-desc valign bgColor-black-000">
	            				<div class="founder-desc text-left">
		            				<h3 class="no-margin color-yellow">Denise Foster</h3>
		            				<p class="no-margin-b color-white">Finance Director</p>
		            			</div>
		            			<div class="founder-socials">
		            				<i class="fa fa-envelope font-25 color-white"></i>
		            			</div>
	            			</div>
	            		</div>
	            	</div>
	            </div>
            </div>
            
		</div>
	</section>

@endsection