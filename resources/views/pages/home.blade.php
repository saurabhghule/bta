
@extends('index')

@section('content')
    
        <section class="section homepage banner-section">
            <div class="home-nav-menu border-t-1">
                <a class="p-lr-15 color-white" href="{{route('home')}}">HOME</a>
                <a class="p-lr-15 color-white" href="{{route('about')}}">ABOUT</a>
                <a class="p-lr-15 color-white" href="#">CONTACT</a>
            </div>

            <div class="bannerSlider m-b-0">
                <div class="slide" id="slide-1">
                    <div class="banner-img"></div>

                    <div class="banner-tagline">
                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="no-margin">
                                    CARRY THE CAUSE
                                </h2>
                                <h5 class="m-tb-20">
                                    We make and sell handbags that raise funding to fight human trafficking.
                                    Each product impacts an initiative that tells you exactly what your purchase
                                    supports, so that when you carry a bag, you Carry the Cause.
                                </h5>
                                <a class="p-15-25 border-1-white color-white cta-btn " href="">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide" id="slide-2">
                    <div class="banner-img"></div>

                    <div class="banner-tagline">

                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="no-margin">
                                    SLAVERY STILL EXISTS
                                </h2>
                                <h5 class="m-tb-20">
                                    Most people think of slavery as a problem of the past.
                                    The upsetting reality is that modern day slavery still occurs
                                    all over the world, even in the U.S and is called human trafficking.
                                </h5>
                                <a class="p-15-25 border-1-white color-white cta-btn" href="">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide" id="slide-3">
                    <div class="banner-img"></div>

                    <div class="banner-tagline">

                        <div class="row">
                            <div class="col-sm-12">
                                <h2 class="no-margin">
                                    THE ARTIST CROSSBODY
                                </h2>
                                <h5 class="m-tb-20">
                                    A casual yet classic staple for the cause makes a bold addition to
                                    any closet. Consider it your go-to bag for feel good vibes that help
                                    spread the word.
                                </h5>
                                <a class="p-15-25 border-1-white color-white cta-btn" href="">LEARN MORE</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slide" id="slide-4">
                    <div class="banner-img"></div>

                    <div class="banner-tagline">
                        <h2 class="no-margin">
                            QUALITY WITH PURPOSE
                        </h2>
                        <h5 class="m-tb-20">
                            All of our products are made in the U.S. or at social impact
                            ventures in developing countries. This means every bag is made
                            under ethical work conditions in places where jobs are greatly needed.
                        </h5>
                        <a class="p-15-25 border-1-white color-white cta-btn" href="">LEARN MORE</a>
                    </div>
                </div>
                <div class="slide" id="slide-5">
                    <div class="banner-img"></div>

                    <div class="banner-tagline">
                        <h2 class="no-margin">
                            UNLEASH HOPE
                        </h2>
                        <h5 class="m-tb-20">
                            The sale of every collar and leash set goes toward placing therapy
                            dogs in human trafficking aftercare homes. These amazing pups provide
                            unconditional love and acceptance to those who need it most.
                        </h5>
                        <a class="p-15-25 border-1-white color-white cta-btn" href="">LEARN MORE</a>
                    </div>
                </div>
            </div>
        </section>

        <section class="section homepage section-1 bgColor-lightGray valign sm-p-tb-75">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="section-content text-left">
                            <h3>Big Team Asia Ltd is a non-profit organization dedicated to fighting human trafficking.</h3>
                            <a href="" class="p-15-25 border-1-black m-t-20 color-black cta-btn ">SUPPORT NOW</a>
                        </div>
                    </div>
                    <!-- <div class="col-sm-12 col-md-6">
                        <div class="about-content-right">
                            <img src="img/home-rose6969.jpg" class="mobile-rose">
                        </div>
                    </div> -->
                </div>
            </div>
        </section>

        <section class="section homepage section-2 p-tb-75">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-header m-b-25">
                            <h3 class="no-margin">FEATURED PRODUCTS</h3>
                        </div>
                    </div>
                </div>

                <div class="product-grid">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <a class="product m-b-25" href="">
                                <div class="product-img">
                                    <img src="/img/home/product-grid/p1.jpg" class="product-img">
                                    <div class="overlay"></div>
                                </div>
                                <div class="product-desc bgColor-yellow">
                                    <h4 class="no-margin bgColor-lightBlack text-left color-white no-margin-b">Product Name</h4>
                                    <div class="cost">
                                        <i class="fa fa-inr color-white" aria-hidden="true"> 500</i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <a class="product m-b-25" href="">
                                <div class="product-img">
                                    <img src="/img/home/product-grid/p2.jpg" class="product-img">
                                    <div class="overlay"></div>
                                </div>
                                <div class="product-desc bgColor-yellow">
                                    <h4 class="no-margin bgColor-lightBlack text-left color-white no-margin-b">Product Name</h4>
                                    <div class="cost">
                                        <i class="fa fa-inr color-white" aria-hidden="true"> 500</i>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-sm-12 col-md-6">
                            <a class="product m-b-25" href="">
                                <div class="product-img">
                                    <img src="/img/home/product-grid/p3.jpg" class="product-img">
                                    <div class="overlay"></div>
                                </div>
                                <div class="product-desc bgColor-yellow">
                                    <h4 class="no-margin bgColor-lightBlack text-left color-white no-margin-b">Product Name</h4>
                                    <div class="cost">
                                        <i class="fa fa-inr color-white" aria-hidden="true"> 500</i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <a class="product m-b-25" href="">
                                <div class="product-img">
                                    <img src="/img/home/product-grid/p4.jpg" class="product-img">
                                    <div class="overlay"></div>
                                </div>
                                <div class="product-desc bgColor-yellow">
                                    <h4 class="no-margin bgColor-lightBlack text-left color-white no-margin-b">Product Name</h4>
                                    <div class="cost">
                                        <i class="fa fa-inr color-white" aria-hidden="true"> 500</i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <a class="product m-b-25" href="">
                                <div class="product-img">
                                    <img src="/img/home/product-grid/p5.jpg" class="product-img">
                                    <div class="overlay"></div>
                                </div>
                                <div class="product-desc bgColor-yellow">
                                    <h4 class="no-margin bgColor-lightBlack text-left color-white no-margin-b">Product Name</h4>
                                    <div class="cost">
                                        <i class="fa fa-inr color-white" aria-hidden="true"> 500</i>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <a class="product m-b-25" href="">
                                <div class="product-img">
                                    <img src="/img/home/product-grid/p6.jpg" class="product-img">
                                    <div class="overlay"></div>
                                </div>
                                <div class="product-desc bgColor-yellow">
                                    <h4 class="no-margin bgColor-lightBlack text-left color-white no-margin-b">Product Name</h4>
                                    <div class="cost">
                                        <i class="fa fa-inr color-white" aria-hidden="true"> 500</i>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section homepage section-3 valign sm-p-tb-75">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <div class="home-initiative-float text-left">
                            <h3 class="no-margin color-black">Bags that make a difference.</h3>
                            <h4 class="m-tb-30 color-black">
                                The Victim Response Bag provides new clothing, 
                                basic hygiene essentials and messages of support 
                                for survivors immediately after they are rescued 
                                from human trafficking situations.
                            </h4>
                            <a class="p-15-25 border-1-black color-black cta-btn" href="">BUY A BAG</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
@endsection