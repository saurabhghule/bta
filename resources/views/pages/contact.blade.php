

@extends('index')


@section('content')

	<section class="contact-page section banner-bg valign">
		<div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="section-content text-center">
                        <h1 class="color-white">Internship Opportunities</h1>
                        <!-- <a href="" class="p-15-25 border-1-black m-t-20 color-black cta-btn ">SUPPORT NOW</a> -->
                        <h4 class="m-t-20 color-white">
                        	When the first Eye Heart World handbags were created, 
                        	we set out to make a statement that would shine a light 
                        	on the plight of millions around the world. We placed a
                        	symbolic orange rose on all of our handbags that represents 
                        	the delicate and unique individual lives of those who are 
                        	exploited. Each life deserves freedom - so when it's in your 
                        	hand or on your shoulder, you don't just carry a bag, you Carry the Cause.
                        </h4>

                        <a class="p-15-25 border-1-white color-white cta-btn m-t-15" href="">APPLY NOW</a>
                    </div>
                </div>
            </div>
        </div>
	</section>

	<section class="contact-page section section-1 p-tb-75">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-md-12">
					<div class="section-header m-b-25">
                        <h3 class="no-margin">CONTACT US</h3>
                    </div>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-12 col-md-4">
					<div class="contact-grid">
						<h3 class="no-margin">Email</h3>
                		<a class="m-t-15 color-black" href="#">
                            <h4 class="no-margin">info@example.com</h4>
                        </a>
					</div>
				</div>
				<div class="col-sm-12 col-md-4">
					<div class="contact-grid">
						<h3 class="no-margin">Phone</h3>
                		<a class="m-t-15 color-black" href="#">
                            <h4 class="no-margin">+91 987654321</h4>
                        </a>
					</div>
				</div>
				<div class="col-sm-12 col-md-4">
					<div class="contact-grid">
						<h3 class="no-margin">Address</h3>
                		<a class="m-t-15 color-black" href="#">
                            <h4 class="no-margin">208 Bhoomi Mall, Sector 15, <br/> CBD Belapur, Navi-Mumbai</h4>
                        </a>
					</div>
				</div>
			</div>

            <form class="m-t-50">
                <div class="form-group no-margin-b">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <input type="text" class="form-control" id="exampleInputName2" placeholder="Name">
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <input type="email" class="form-control" placeholder="Email">
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <input type="tel" class="form-control" placeholder="Phone number">
                        </div>

                        <div class="col-sm-12">
                            <input type="text" class="form-control" placeholder="Subject">
                        </div>

                        <div class="col-sm-12">
                            <textarea class="form-control" rows="5" placeholder="What Questions do you have?"></textarea>
                        </div>

                        <a class="p-15-25 border-1-black color-black cta-btn" href="">SUBMIT</a>
                    </div>
                </div>
            </form>
		</div>
	</section>

@endsection