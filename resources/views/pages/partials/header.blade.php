<!-- MAIN NAVIGATION -->

<nav id="nav-main">
    <div class="nav-logo">Logo Here</div>
    <ul class="nav-links">
        <li><a href="{{route('home'}}" >Home</a></li>
        <li><a href="{{route('about')}}" >About</a></li>
        <li><a href="{{route('contact')}}" >Contact</a></li>
    </ul>
</nav>

<!-- MOBILE NAVIGATION -->
<nav id="mobile-navigation">
    <div id="nav-mobile">
        <div id="mobile-logo"><a href="index.html" >Logo Here</a></div>
        <div id="mobile-button"><div class="line"></div><div class="line"></div><div class="line"></div></div>
    </div>
    <div id="nav-links">
        <a href="{{route('home')}}">Home</a>
        <a href="{{route('about')}}">About</a>
        <a href="{{route('contact')}}">Contact</a>
    </div>
</nav>