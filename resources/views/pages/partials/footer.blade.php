
<!-- FOOTER -->

<section class="footer bgColor-black-333" id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-12">
                <div class="footer-navigation p-tb-75">
                    <div class="row">
                        <div class="col-md-6 col-sm-12 col-xs-12 links-grid">
                            <h3 class="color-yellow m-b-20">Other</h3>
                            <a class="m-b-10" href="#">
                                <h4 class="no-margin">My Account</h4>
                            </a>
                            <a class="m-b-10" href="#">
                                <h4 class="no-margin">Articles & Events</h4>
                            </a>
                            <a class="m-b-10" href="#">
                                <h4 class="no-margin">FAQ</h4>
                            </a>
                            <a class="m-b-10" href="#">
                                <h4 class="no-margin">Privacy</h4>
                            </a>
                            <a class="m-b-10" href="#">
                                <h4 class="no-margin">Terms of Use</h4>
                            </a>
                        </div>
                        <div class="col-md-6  col-sm-12 col-xs-12 links-grid">
                            <h3 class="color-yellow m-b-20">Contact Us</h3>
                            <form action="" method="post" novalidate>
                                <div id="">
                                    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="E-mail Address" required><input type="submit" value="GO" name="subscribe" id="mc-embedded-subscribe" class="subscribe-button">
                                    <div style="position: absolute; left: -5000px;"><input type="text" tabindex="-1" value=""></div>
                                </div>
                            </form>

                            <a href=""><i class="fa fa-facebook"></i></a>
                            <a href=""><i class="fa fa-twitter"></i></a>
                            <a href=""><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-info">
        <div class="copyright"><p>&copy; 2015, Big Team Asia Ltd</p></div>
        <a href="#" class="top-button"><i></i></a>
    </div>
</section>