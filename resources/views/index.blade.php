<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Big Team Asia Ltd</title>

	<link href="/css/app.css" rel="stylesheet">

	<!-- Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Josefin+Sans:400,600' rel='stylesheet' type='text/css'>


</head>
<body>
<!-- LOADING OVERLAY -->

{{--<div id="overlay">--}}
    {{--<div id="logo-anim-container">--}}
        {{--<h2>Loading...</h2>--}}
    {{--</div>--}}
{{--</div>--}}


@yield('content')

@include('pages.partials.footer')

<!-- Scripts -->
<script type="text/javascript" src="/js/all.js"></script>
</body>
</html>
