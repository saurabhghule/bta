

$(document).ready(function(){
    //$('#banner-slider').carousel({
    //    interval: 8000,
    //    wrap:true
    //})


    $('.bannerSlider').slick({
        arrows: true,
        autoplay: true,
        pauseOnHover: false,
        autoplaySpeed: 8000,
        speed: 300,
        cssEase: 'ease',
        fade:true,
        infinite: true,
        lazyLoad: "ondemand"
        //prevArrow: '<span class="nav-prev"><i class="fa fa-angle-left"></i></span>',
        //nextArrow: '<span class="nav-next"><i class="fa fa-angle-right"></i></span>'
    });
});