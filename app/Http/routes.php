<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'WebsiteController@index']);
Route::get('/about', ['as' => 'about', 'uses' => 'WebsiteController@about']);
//Route::get('/product', ['as' => 'product', 'uses' => 'WebsiteController@product']);
Route::get('/contact', ['as' => 'contact', 'uses' => 'WebsiteController@contact']);
