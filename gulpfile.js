var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = true;

elixir(function (mix) {
    mix.sass('app.scss')
        .scripts([
            'vendor/jquery.min.js',
            'vendor/modernizr.js',
            'vendor/bootstrap.min.js',
            'vendor/isotope.pkgd.min.js',
            'vendor/slick.min.js',
            'vendor/jquery.stellar.min.js',
            'vendor/waypoints.min.js',
            'vendor/jquery.nicescroll.min.js',
            'script.js'
        ],'public/js/all.js', 'resources/assets/js');
});
